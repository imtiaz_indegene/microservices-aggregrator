package com.indegene.aggregrator;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by imirza on 12/21/2016.
 */
@RestController
@RequestMapping(value = "/aggregation", produces = "application/json")
public class AggregationResource {

    // Uses Ribbon to load balance requests

   private RestTemplate loadBalancedRestTemplate;



    @RequestMapping(value = "/api/v1/files/{fileuuid:.+}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity getFileContents(HttpServletResponse response,
                                          @PathVariable("fileuuid") String fileNameWithUUID) {
        String url = "http://FILESERVICE/api/v1/files/"+fileNameWithUUID;
        return this.loadBalancedRestTemplate.getForObject(url, null, fileNameWithUUID);
}

   /* @RequestMapping(value = "/api/v1/files/upload",
                    method = RequestMethod.POST)
    public HashMap handleFileUpload(@RequestParam("file") MultipartFile file) throws IOException {

        String url = "http://FILESERVICE/api/v1/files/upload";
        LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();

        File newFile = new File(file.getOriginalFilename());
                OutputStream outputStream = new FileOutputStream(newFile);
        IOUtils.copy(file.getInputStream(), outputStream);
        outputStream.close();

        map.add("file",new FileSystemResource(newFile));


        return this.loadBalancedRestTemplate.postForObject(url,  map, HashMap.class);


    }*/

    //TODO Check for performance bottleneck in this method
    @RequestMapping(value = "/api/v1/files/upload",
            method = RequestMethod.POST)
    public HashMap handleFileUpload(@RequestParam("file") MultipartFile file) throws IOException {

        String url = "http://FILESERVICE/api/v1/files/upload";
        LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();

        HttpHeaders filePartHeaders = new HttpHeaders();
        filePartHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);

        ByteArrayResource multipartFileResource = new ByteArrayResource(file.getBytes());
        MultipartFileResource mfr = new MultipartFileResource(multipartFileResource,file.getOriginalFilename());

        HttpEntity<Resource> filePart = new HttpEntity<>(mfr, filePartHeaders);

        map.add("file", filePart );

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity
                = new HttpEntity<LinkedMultiValueMap<String, Object>>(
                map, headers);


        return this.loadBalancedRestTemplate.postForObject(url,  map, HashMap.class);

    }



    @RequestMapping(value = "/webcontent/v1", method = RequestMethod.GET)
    @ResponseBody
    public void getDefault(HttpServletResponse response) throws Exception {
        String url = "http://FILESERVICE/webcontent/v1";
        this.loadBalancedRestTemplate.getForObject(url, HttpServletResponse.class);
    }



    @ApiOperation(value = "getGreeting", nickname = "getGreeting")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = AggregationResource.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String sayHello() {
        String url = "http://FILESERVICE/hello";
        return this.loadBalancedRestTemplate.getForObject(url, String.class);
    }

    @Autowired
    public void setLoadBalancedRestTemplate(RestTemplate loadBalancedRestTemplate) {


        this.loadBalancedRestTemplate = loadBalancedRestTemplate;
    }

    static class MultipartFileResource extends ByteArrayResource {

        private final String filename;

        public MultipartFileResource(byte[] payload, String originalFileName) throws IOException {
            super(payload);
            this.filename = originalFileName;
        }

        public MultipartFileResource(ByteArrayResource bas, String originalFileName) throws IOException {
            super(bas.getByteArray());
            this.filename = originalFileName;
        }

        @Override
        public String getFilename() {
            return this.filename;
        }
    }
}